Configus.build Rails.env do
  env :production do
    faye_url ENV['FAYE_URL']
  end

  env :development, parent: :production do
  end
  
  env :test, parent: :production do
    faye_url nil
  end
end

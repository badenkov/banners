Rails.application.routes.draw do

  namespace :api do
    namespace :v1 do
      resource :random_banners, only: [:show]
      resources :banners, only: [] do
        get :image, on: :member
        get :url, on: :member
      end
    end
  end

  scope module: :web do
    root to: "welcome#index"
    resources :banners, only: [:index, :new, :create, :edit, :update, :destroy]
  end
end

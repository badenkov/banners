require 'spec_helper'

describe Web::BannersController do
  render_views

  let(:banner) { create :banner }

  describe "GET #index" do
    before do
      create_list :banner, 5
    end
    it "responds success" do
      get :index

      expect(response).to be_success
      expect(response.status).to eq(200)
    end
  end

  describe "GET #new" do
    it "responds success" do
      get :new

      expect(response).to be_success
      expect(response.status).to eq(200)
    end
  end

  describe "GET #edit" do
    it "responds success" do
      get :edit, id: banner.id

      expect(response).to be_success
      expect(response.status).to eq(200)
    end
  end

  describe "POST #create" do
    it "creates new banner" do
      attrs = attributes_for :banner

      post :create, banner: attrs

      expect(response).to redirect_to(banners_path)
      expect { Banner.find_by(title: attrs.title) }.to be
    end
  end

  describe "PATCH #update" do
    let(:attrs) { attributes_for :banner }
    before do
      patch :update, id: banner.id, banner: attrs
    end

    it "update the banner" do
      b = Banner.find(banner.id)
      expect(b.title).to eq(attrs[:title])
    end

    it "redirect to banners page" do
      expect(response).to redirect_to(banners_path)
    end
  end

  describe "DELETE #destroy" do
    it "delete the banner" do
      banner

      expect {
        delete :destroy, id: banner.id
      }.to change(Banner, :count).by(-1)
    end

    it "redirect to banners page" do
      delete :destroy, id: banner.id

      expect(response).to redirect_to(banners_path)
    end
  end

end

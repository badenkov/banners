require 'spec_helper'

describe Api::V1::BannersController do
  render_views

  let (:banner) { create :banner }

  describe "GET #image" do
    it "responds success" do
      get :image, id: banner.id
      
      expect(response).to be_success
      expect(response.status).to eq(200)
    end
  end

  describe "GET #url" do
    it "redirect to banner url" do
      get :url, id: banner.id

      expect(response).to redirect_to(banner.url)
      expect(response.status).to eq(302)
    end
  end
end

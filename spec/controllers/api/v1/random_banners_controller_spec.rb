require 'spec_helper'

describe Api::V1::RandomBannersController do
  render_views

  let(:banner) { create :banner }

  describe "GET #show" do
    it "responds success" do
      get :show

      expect(response).to be_success
      expect(response.status).to eq(200)
    end
  end
end

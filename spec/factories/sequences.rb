FactoryGirl.define do
  sequence :title do |n|
    "title_#{n}"
  end

  sequence :url do |n|
    "http://url_#n.ru"
  end

  sequence :image do |n|
    Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/spec/fixtures/image.jpg')))
  end
end

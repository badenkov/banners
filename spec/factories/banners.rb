# == Schema Information
#
# Table name: banners
#
#  id          :integer          not null, primary key
#  title       :string(255)
#  url         :string(255)
#  image       :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#  show_count  :integer          default(0)
#  click_count :integer          default(0)
#

FactoryGirl.define do
  factory :banner do
    title
    url
    image
  end
end

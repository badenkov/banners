class AddShowAndClickCountFieldsToBanner < ActiveRecord::Migration
  def change
    add_column :banners, :show_count, :integer, default: 0
    add_column :banners, :click_count, :integer, default: 0
  end
end

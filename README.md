# README

Банерокрутилка

## Системные требования

Приложение разрабатывалось и тестировалось

* ruby >= 2.0
* postgresql 9.3

На другом ПО или других версиях используйте на свой страх и риск

## Конфигурирование

Конфигурирование производится через переменные окружения

    DATABASE_URL
    FAYE_URL

Для **production** окружения еще необходим

    SECRET_KEY_BASE

Разработчику рекомендуется использоват .env.development и .env.test файлы
для объявления переменных окружения. Благодаря гему dotenv приложение подхватит
их автоматически при запуске. Пример **.env.development**

    DATABASE_URL: postgresql://postgres@localhost/banners_development?pool=5
    FAYE_URL: http://172.28.128.10:9292/faye

## Инструкция по развертыванию

Клонируем репозиторий с кодом

    git clone <repo> ./banners
    cd banners

Настраиваем виртуальную машину

    cp Vagrantfile.example Vagrantfile
    vagrant up --provision

В дирректории cm находятся _ansible_ плейбуки, которые установят и настроят все
необходимое для работы приложения.

    vagrant ssh
    cd /vagrant

Затем устанавливаем гемы

    bundle install

Создаем базу данных

    bundle exec db:setup

Для запуска тестов

    bundle exec rake db:test:prepare
    bundle exec rake spec

Для генерирования отчета SimpleCov

    bundle exec rake spec:coverage

Для запуска приложения

    bundle exec foreman start

## Нагрузочное тестрирование

  в процессе

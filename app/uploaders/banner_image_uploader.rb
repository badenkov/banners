class BannerImageUploader < ApplicationUploader
  include CarrierWave::MiniMagick

  process resize_to_fit: [200, 200]

  def extension_white_list
    %w(jpg jpeg gif png)
  end
end

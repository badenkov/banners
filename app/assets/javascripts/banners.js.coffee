client = new Faye.Client(gon.faye_url)

subscription = client.subscribe('/banners/update', (message) ->
  bannerId = "#banner_" + message.id
  banner = $(bannerId)
  banner.find('.js-show_count').text(message.show_count)
  banner.find('.js-click_count').text(message.click_count)
  banner.find('.js-conversion').text(message.conversion)
)

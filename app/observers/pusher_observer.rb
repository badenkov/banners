class PusherObserver < ActiveRecord::Observer
  observe :banner

  def after_update(record)
    data = {
      id: record.id,
      show_count: record.show_count,
      click_count: record.click_count,
      converson: record.conversion
    }
    FayeService.publish('/banners/update', data)
  end
end

class Api::V1::RandomBannerSerializer < ApplicationSerializer
  self.root = false

  attributes :title, :url, :image_url

  def url
    url_api_v1_banner_url(id: object.id)
  end

  def image_url
    image_api_v1_banner_url(id: object.id)
  end
end

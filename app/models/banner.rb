# == Schema Information
#
# Table name: banners
#
#  id          :integer          not null, primary key
#  title       :string(255)
#  url         :string(255)
#  image       :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#  show_count  :integer          default(0)
#  click_count :integer          default(0)
#

class Banner < ActiveRecord::Base

  mount_uploader :image, BannerImageUploader
  
  validates :title, presence: true
  validates :url, presence: true

  include BannerRepository

  def conversion
    click_count.to_f / show_count.to_f
  end
end

class BannerForm < Reform::Form
  include Reform::Form::ActiveRecord

  property :title
  property :url
  property :image
end

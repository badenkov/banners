class Api::V1::BannersController < Api::V1::ApplicationController

  def image
    @banner = Banner.find(params[:id])
    @banner.show_count += 1
    @banner.save

    send_file @banner.image.path, disposition: "inline"
  end

  def url
    @banner = Banner.find(params[:id])
    @banner.click_count += 1
    @banner.save

    redirect_to @banner.url, status: :found
  end
end

class Api::V1::RandomBannersController < Api::V1::ApplicationController
  def show
    offset = rand(Banner.count)
    @banner = Banner.offset(offset).first

    render json: @banner, callback: params[:callback], serializer: Api::V1::RandomBannerSerializer
  end
end

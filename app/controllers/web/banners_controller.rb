class Web::BannersController < Web::ApplicationController
  def index
    @banners = Banner.all
  end

  def new
    @banner = Banner.new
    @form = BannerForm.new(@banner)
  end

  def create
    @banner = Banner.new
    @form = BannerForm.new(@banner)

    if @form.validate(params[:banner])
      @form.save
      redirect_to banners_path
    else
      render :new
    end
  end

  def edit
    @banner = Banner.find(params[:id])
    @form = BannerForm.new(@banner)
  end

  def update
    @banner = Banner.find(params[:id])
    @form = BannerForm.new(@banner)

    if @form.validate(params[:banner])
      @form.save
      redirect_to banners_path
    else
      render :edit
    end
  end

  def destroy
    @banner = Banner.find(params[:id])
    @banner.destroy
    
    redirect_to banners_path
  end

end
